let grid_width, grid_height;
const size_cell = 30;
const COULEUR = ["#EEE", "#F00", "#0F0", "#111"];
let world = [];
let snakeTab = [];
let direction;
var interval;
let score = 0;

const foodImage = new Image();
foodImage.src = './assets/nourriture.png';
const wallImage = new Image();
wallImage.src = './assets/wall.jpg';
const snakeImage = new Image();
snakeImage.src = './assets/snake.jpg';


const initWorld = (gameInfo) => {
  
  snakeTab = [];
  score = 0;
  world = [];
  direction = "ArrowRight";
  grid_width = gameInfo.dimensions[0];
  grid_height = gameInfo.dimensions[1];
  let i, j;

  // GRID INIT
  for(i = 0; i < gameInfo.dimensions[0]; i++){
    let emptyLine = [];
    for(j = 0; j < gameInfo.dimensions[1]; j++){
      emptyLine.push("EMPTY");
    }
    world.push(emptyLine);
  }

  // WALL INIT
  for(let walls of gameInfo.walls){
    world[walls[0]][walls[1]] = "WALL";
  }

  // FOOD INIT
  for(let food of gameInfo.food){
    world[food[0]][food[1]] = "FOOD";
  }

  // SNAKE INIT
  snakeTab = [...gameInfo.snake];

  interval = setInterval(step, gameInfo.delay);

  return world;
}

const drawGrid = () => {
  const canvas = document.getElementById("canvas");
  const context = canvas.getContext('2d');

  document.getElementById('score').innerHTML = score;

  canvas.width = grid_width * size_cell;
  canvas.height = grid_height * size_cell;

  // dessin du quadrillage
  context.fillStyle = "white";
  for (let i = 0; i <= grid_width; i++) {
      context.fillRect(size_cell * i, 0, 1, canvas.height); // lignes verticales
  }
  for (let i = 0; i <= grid_height; i++) {
      context.fillRect(0, size_cell * i, canvas.width, 1); // lignes horizontales
  }
  
  world.forEach((line, x) => {
    line.forEach((type, y) => {
      switch(type) {
        case "FOOD":
          context.drawImage(foodImage, x * size_cell + 1, y * size_cell + 1, size_cell - 1, size_cell - 1);
          break;
        case "WALL":
          context.drawImage(wallImage, x * size_cell + 1, y * size_cell + 1, size_cell - 1, size_cell - 1);
          break;
      }
    });
  });

  snakeTab.forEach((pos) => {
    context.drawImage(snakeImage, pos[0] * size_cell + 1, pos[1] * size_cell + 1, size_cell - 1, size_cell - 1);
  });
}

const newHead = () => {
  let oldHead = snakeTab.slice(-1)[0];
  let head;
  switch (direction) {
    case "ArrowRight":
      head = [oldHead[0] + 1, oldHead[1]];
      break;
    case "ArrowDown":
      head = [oldHead[0], oldHead[1] + 1];
      break;
    case "ArrowLeft": 
      head = [oldHead[0] - 1, oldHead[1]];
      break;
    case "ArrowUp":
      head = [oldHead[0], oldHead[1] - 1];
      break;
  }
  return head;
}

const eat = () => {
  let head = snakeTab.slice(-1)[0];
  if (world[head[0]][head[1]] === "FOOD") {
      world[head[0]][head[1]] = "EMPTY";
      score += 1;
      // REGENERER NOURRITURE
      x = Math.floor(Math.random() * grid_height);
      y = Math.floor(Math.random() * grid_width);
      while (world[x][y] !== "EMPTY") {
        x = Math.floor(Math.random() * grid_height);
        y = Math.floor(Math.random() * grid_width);
      }
      world[x][y] = "FOOD";
      return true;
  }
  return false;
}


const step = () => {
  const head = newHead();
  if (head[0] < 0 || head[1] < 0 || head[0] >= grid_width || head[1] >= grid_height || snakeTab.find(elem => elem[0] == head[0] && elem[1] == head[1]) != undefined || world[head[0]][head[1]] === "WALL") { 
      clearInterval(interval);
      alert("You are dead.\nYour score is : " + score);
      endGame();
  } else {
      if (!eat()) {
        snakeTab.shift();
      }
      snakeTab.push(head);
  }
  drawGrid();
}


document.addEventListener("keydown", (event) => {
  if (["ArrowDown", "ArrowUp", "ArrowLeft", "ArrowRight"].includes(event.key)) {
    direction = event.key;
  }
});

