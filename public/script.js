let levels;

// GENERATION DE LA PAGE (bloquer évenement tant que non chargé)
window.addEventListener("load", async() => {
  levels = await getLevels();

  let last_item = document.getElementById("left");
  var increment = 1;
  
  //POUR GERER DYNAMIQUEMENT L'AJOUT ET LA SUPPRESSION DE NIVEAU
  //DEPUIS LE FICHIER JSON
  for(const level of levels.levels) {
    const container = document.createElement('div');
    const text = document.createElement('p');
    text.innerHTML = level.name;
    container.appendChild(text);  
    container.type = "button";
    container.id = "Niveau"+ increment ;
    container.addEventListener('click', () => launchGame(level));
    last_item.after(container);
    last_item = container;
    increment = increment+1;
  }
  
  document.body.style.pointerEvents = "all";
});


const launchGame = (gameInfo) => {
  document.getElementById('gameMenu').style.display = "none";
  initWorld(gameInfo);
  drawGrid();
}

const endGame = () => {
  document.getElementById('gameMenu').style.display = "block";
}

const getLevels = () => {
  return new Promise(function (resolve, reject) {
      const req = new XMLHttpRequest();
      req.open('GET', `./assets/gameLevels.json`);
      req.onload = function () {
          if (this.status >= 200 && this.status < 300) {
              resolve(JSON.parse(req.responseText));
          } else {
              reject({status: this.status,
                statusText: "Échec de chargement des données"
              });
          }
      };
      req.onerror = function () {
          reject({
              status: this.status,
              statusText: "Échec de chargement des données"
          });
      };
      req.send();
  });
}